﻿using System;

namespace Unibo.Oop.Utils
{
    public interface ITuple
    {
        int Length { get; }
        Object[] ToArray();
        Object this[int i] { get; }
    }
}

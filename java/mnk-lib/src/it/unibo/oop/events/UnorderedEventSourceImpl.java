package it.unibo.oop.events;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

class UnorderedEventSourceImpl<T> extends AbstractEventSourceImpl<T> {
    private Set<EventListener<T>> eventListeners = new HashSet<>();

    @Override
    protected Collection<EventListener<T>> getEventListeners() {
        return eventListeners;
    }
}

package it.unibo.oop.events;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

class OrderedEventSourceImpl<T> extends AbstractEventSourceImpl<T> {
    private List<EventListener<T>> eventListeners = new LinkedList<>();

    @Override
    protected Collection<EventListener<T>> getEventListeners() {
        return eventListeners;
    }
}
